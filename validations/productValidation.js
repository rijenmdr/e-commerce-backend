const { check , validationResult } =  require("express-validator");

exports.productIdValidation = [
    check('productId')
    .not()
    .isEmpty()
    .withMessage("Product Id cannot be empty"),
    (req,res,next) =>{  
        const error = validationResult(req);
        if(!error.isEmpty()){
            return res.status(422).json({
                status:"fail",
                error:error.array()
            });
        }
        next()
    }
]

exports.cartValidation = [
    check('productId')
    .not()
    .isEmpty()
    .withMessage("Product Id cannot be empty"),
    check('numberOfItem')
    .not()
    .isEmpty()
    .withMessage("Number of products cannot be empty")
    .isNumeric()
    .withMessage("Number of products can be composed of only number"),
    (req,res,next) =>{  
        const error = validationResult(req);
        if(!error.isEmpty()){
            return res.status(422).json({
                status:"fail",
                error:error.array()
            });
        }
        next()
    }
]