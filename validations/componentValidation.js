const { check } =  require("express-validator");

exports.emailValidation = () => {
    return  check('email')
    .trim()
    .isEmail()
    .normalizeEmail()
    .withMessage('Invalid email address!')
}

exports.nameValidation = () => {
    return  check('name')
    .trim()
    .escape()
    .isLength({min: 2})
    .withMessage('Name must be atleast length of 2!')
}

exports.passwordValidation = () => {
    return check('password')
        .matches( /^(?=.*[A-Za-z])(?=.*\d)[\w\d\s#?!,.;:"'|`~=_/()@$%^&*+-]{6,}$/)
        .withMessage('Password must be minimum of 6 characters with number and letters')
}

exports.mobileValidation = () => {
    return check('mobileNo')
        .matches(/^\b((98(0|1|2)[0-9]{7})|(9(7|8)(4|6)[0-9]{7})|(98(4|6)[0-9]{7})|(985[0-9]{7})|(97(4|5)[0-9]{7})|(972[0-9]{7})|(9(88|(6(1|2)))[0-9]{7}))\b$/)
        .withMessage('Invalid Mobile Number')
}