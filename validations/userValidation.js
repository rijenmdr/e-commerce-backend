const { check, validationResult } = require('express-validator');
const { passwordValidation, mobileValidation, emailValidation } = require('./componentValidation');

const moment = require('moment');

exports.loginValidation = [
    check('loginId')
        .trim()
        .not()
        .isEmpty()
        .withMessage("Login ID shouldnot be empty"),
    passwordValidation(),
    check('fcmToken')
        .custom((value, { req }) => {
            if (value !== "4274" && value !== "4275") {
                throw new Error('Invalid fcm token')
            } else {
                return true
            }
        }),
    (req, res, next) => {
        const error = validationResult(req);
        if (!error.isEmpty()) {
            return res.status(422).json({
                status: "fail",
                error: error.array()
            });
        }
        next()
    }
]

exports.registerValidation = [
    check('firstName')
        .trim()
        .escape()
        .not()
        .isEmpty()
        .withMessage('Firstname can not be empty!'),
    check('lastName')
        .trim()
        .escape()
        .not()
        .isEmpty()
        .withMessage('Lastname can not be empty!'),
    emailValidation(),
    passwordValidation(),
    mobileValidation(),
    check('gender')
        .custom((value, { req }) => {
            if (value !== 'm' && value !== "f" && value !== "o") {
                throw new Error('Invalid gender id')
            } else {
                return true
            }
        }),
    check('country')
        .trim()
        .escape()
        .not()
        .isEmpty()
        .withMessage('Country can not be empty!'),
    check('city')
        .trim()
        .escape()
        .not()
        .isEmpty()
        .withMessage('City can not be empty!'),
    check('street')
        .trim()
        .escape()
        .not()
        .isEmpty()
        .withMessage('Street can not be empty!'),
    check('dob')
        .custom((value, { req }) => {
            if (moment().diff(moment(value), 'years') <= 16) {
                throw new Error('User must be older than 16 years old')
            } else {
                return true
            }
        }),
    (req, res, next) => {
        const error = validationResult(req);
        if (!error.isEmpty()) {
            return res.status(422).json({
                status: "fail",
                error: error.array()
            });
        }
        next()
    }
]

exports.mobileExistValidation = [
    mobileValidation(),
    (req, res, next) => {
        const error = validationResult(req);
        if (!error.isEmpty()) {
            return res.status(422).json({
                status: "fail",
                error: error.array()
            });
        }
        next()
    }
]

exports.changePasswordValidation = [
    mobileValidation(),
    passwordValidation(),
    (req, res, next) => {
        const error = validationResult(req);
        if (!error.isEmpty()) {
            return res.status(422).json({
                status: "fail",
                error: error.array()
            });
        }
        next()
    }
]