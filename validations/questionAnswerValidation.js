const {check,validationResult} = require('express-validator');

exports.questionValidation = [
    check('productId')
    .not()
    .isEmpty()
    .withMessage("Product Id cannot be empty"),
    check('question')
    .not()
    .isEmpty()
    .withMessage("Question cannot be empty"),
    (req,res,next) =>{  
        const error = validationResult(req);
        if(!error.isEmpty()){
            return res.status(422).json({
                status:"fail",
                error:error.array()
            });
        }
        next()
    }
]

exports.replyValidation = [
    check('questionId')
    .not()
    .isEmpty()
    .withMessage("Question Id cannot be empty"),
    check('reply')
    .not()
    .isEmpty()
    .withMessage("Reply cannot be empty"),
    (req,res,next) =>{  
        const error = validationResult(req);
        if(!error.isEmpty()){
            return res.status(422).json({
                status:"fail",
                error:error.array()
            });
        }
        next()
    }
]