const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    mobileNo: {
        type: String,
        // required: true
    },
    address: {
        country: {
            type: String,
            // required: true
        },
        city: {
            type: String,
            // required: true
        },
        street: {
            type: String,
            // required: true
        }
    },
    password: {
        type: String,
        // required: true
    },
    dob: {
        type: Date,
        // required: true
    },
    gender: {
        type: String,
        // required: true,
        // default:'o'
    },
    role: {
        type: Number,
        required: true
    },
    profileImage: {
        type: String,
    },
    isVerified: {
        type: Boolean,
        required: true
    },
    cart: [
        {
            id: {
                type: Schema.Types.ObjectId,
                ref: "Product",
                required: true
            },
            name: {
                type: String,
                required: true
            },
            previewImg: {
                type: String,
                required: true
            },
            averageRating: {
                type: String,
                required: true,
                default: 0
            },
            actualPrice: {
                type: Number,
                required: true,
            },
            onStock: {
                type: Number,
                required: true,
            },
            soldIn: {
                type: String,
                required: true
            },
            shippingCost: {
                type: Number,
                required: true,
            },
            numberForDelivery: {
                type: Number,
                required: true
            },
            timeForDelivery: {
                type: String,
                required: true
            },
            numberOfItem: {
                type: Number,
                required: true
            }
        }        
    ],
    favouriteProducts: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product'
        }
    ],
    isGoogleUser: {
        type: Boolean,
        default: false
    },
    blogs: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Blog'
        }
    ],
    reviews: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'ProductReview'
        }
    ],
    questions: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'QuestionAnswer'
        }
    ]
}, { timestamps: true });

module.exports = mongoose.model('User', userSchema)