const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const orderSchema = new Schema({
    productIds:[
        {
            type: Schema.Types.ObjectId,
            ref: 'Product',
            required: true
        }
    ],
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    zip: {
        type: String,
        required: true
    },
    mobile: {
        type: String,
        required: true
    },
    additionalInformation: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    expectedDate: {
        type: String,
        require: true
    },
    hasPaid: {
        type: Boolean,
        required: true,
        default: false
    },
    paymentMethod: {
        type: String,
        required: true
    },
    hasCanceled: {
        type: Boolean,
        required: true,
        default: false
    },
    cancelledDate: {
        type: Date,
        required: true
    },
    reason: {
        type: String,
        required: true
    }
}, { timestamps: true });

module.exports = mongoose.model('Order', orderSchema)