const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const testimonySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    profileImage: {
        type: String,
        required: true
    },
    quote: {
        type: String,
        required: true
    },
}, { timestamps: true });

module.exports = mongoose.model('Testimony', testimonySchema);
