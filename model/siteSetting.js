const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const siteSettingSchema = new Schema({
    nav: [
        {
            categoryId: {
            type: Schema.Types.ObjectId,
            ref: 'Category',
            // required: true
            },
            products: [
                {
                    type: String,
                    required:true
                }
            ]
        }
    ],
    categoryMenu: [
        {
            type: Schema.Types.ObjectId,
            ref: "Category"
        }
    ],
    carouselImages: [
        {
            type: String,
            required: true
        }
    ],
    bestSellingCategory: [
        {
            type: Schema.Types.ObjectId,
            ref: "Category"
        }
    ],
    bestSellingProducts: [
        {
            type: Schema.Types.ObjectId,
            ref: "Product"
        }
    ],
    featuredTitle: {
        type: String,
        required: true
    },
    featuredProductTitles: [
        {
            type: String,
            required:true
        }
    ],
    featuredProduct: [
        {
            type: Schema.Types.ObjectId,
            ref: "Product"
        }
    ],
    testimony: [
        {
            person: {
                type: String,
                required: true
            },
            quote: {
                type: String,
                required: true
            },
            profileImage: {
                type: String,
                required: true
            },
        }
    ],
    popularProduct: [
        {
            type: Schema.Types.ObjectId,
            ref: "Product"
        }
    ],
    primaryFeaturedBlogs: [
        {
            type: Schema.Types.ObjectId,
            ref: "Blog"
        }
    ],
    secondaryFeaturedBlogs: [
        {
            type: Schema.Types.ObjectId,
            ref: "Blog"
        }
    ],
    productTags: [
        {
            type: String,
            required:true
        }
    ],
}, { timestamps: true });

module.exports = mongoose.model('SiteSetting', siteSettingSchema);
