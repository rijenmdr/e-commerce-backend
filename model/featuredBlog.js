const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const featuredBlogSchema = new Schema({
    blogId: {
        type: Schema.Types.ObjectId,
        ref: 'Blog',
        required: true
    },
    type: {
        type: String,
        required: true
    },
    priority: {
        type: Number,
        required: true
    }
}, { timestamps: true });

module.exports = mongoose.model('FeaturedBlog', featuredBlogSchema);
