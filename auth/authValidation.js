const jwt = require('jsonwebtoken');

exports.authValidation = async (req, res, next) => {
    const authHeader = req.get('Authorization');
    if (!authHeader) {
        return res.status(401).json({
            status: 401,
            statusMessage: "fail",
            message: "UnAuthenticated"
        })
    }

    const [bearer, token] = authHeader.split(' ');

    if (bearer !== "Bearer") {
        return res.status(403).json({
            status: 403,
            statusMessage: "fail",
            message: "Invalid token type"
        })
    }

    try {
        const user = await jwt.verify(token, process.env.JWT_SECRET_KEY);
        console.log(user)
        if (!user) {
            return res.status(401).json({
                status: 401,
                statusMessage: "fail",
                message: "UnAuthenticated"
            })
        }
        req.user = user
        next();
    } catch (err) {
        res.status(401).json({
            status: 401,
            statusMessage: "fail",
            message: "UnAuthenticated"
        })
    }
    
}