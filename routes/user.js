const express = require('express');
const { authValidation } = require('../auth/authValidation');

const userController = require('../controller/user');
const orderController = require('../controller/order');
const productController = require('../controller/product'); 
const productReviewController = require('../controller/productReview');

const { loginValidation, registerValidation, mobileExistValidation, changePasswordValidation } = require('../validations/userValidation');

const router = express.Router();

router.post('/upload-avatar', authValidation, userController.uploadProfilePicture)

//reviews
router.get('/reviews', authValidation, productReviewController.getAllUserReviews);

//wishlist
router.get('/my-wishlist', authValidation, productController.getUserFavouriteProducts)

//order
router.post('/cancel-order', authValidation, orderController.cancelOrder)

router.get('/all-product-purchases', authValidation, orderController.getAllUserPurchasedProducts)

router.get('/product-purchases', authValidation, orderController.getUserPurchases)

router.post('/update-profile', authValidation, userController.updateUserProfile)

router.get('/current-user', authValidation, userController.getCurrentUser)

router.post('/forget-password', changePasswordValidation, userController.forgetPassword);

router.post('/mobile-verify', mobileExistValidation, userController.userExists);

router.post('/google-login', userController.loginWithGoogle)

router.post('/register', registerValidation, userController.registerUser);

router.post('/login', loginValidation, userController.loginUser);

module.exports = router;