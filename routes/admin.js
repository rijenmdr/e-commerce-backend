const express = require('express');

const adminController = require('../controller/admin');

const router = express.Router();

router.post('/add-testimony', adminController.addTestimony);

router.get('/home', adminController.getHomeDetails);

// router.post('/add-setting', siteSettingController.addSiteSettings);

module.exports = router;