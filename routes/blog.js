const express = require('express');

const blogController = require('../controller/blog');
const blogCommentController = require('../controller/blogComment');
const { commentValidation } = require('../validations/commentValidation');

const router = express.Router();

//featuredBlog
router.get('/get-secondary-feature', blogController.getSecondaryFeaturedBlogs)

router.post('/add-featured-blog', blogController.addBlogToFeatured);

//comments
router.get('/get-blog-comment/:blogId', blogCommentController.getCommentById);

router.post('/add-comment', commentValidation, blogCommentController.addComment);

//blog
router.post('/add', blogController.addBlog);

router.get('/get-blogs', blogController.getBlogs);

router.get('/get-blogs-tags', blogController.getBlogsByTag);

router.get('/get-blog-detail/:blogId', blogController.getBlogDetail);

module.exports = router;