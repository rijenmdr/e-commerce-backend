const express = require('express');

const productController = require('../controller/product');
const brandController = require('../controller/brand');
const questionAnswerController = require('../controller/questionAnswer');
const productReviewController = require('../controller/productReview');
const orderController = require('../controller/order');

//validation
const { questionValidation, replyValidation } = require('../validations/questionAnswerValidation');
const { authValidation } = require('../auth/authValidation');
const { productIdValidation, cartValidation } = require('../validations/productValidation');

const router = express.Router();

//order 
router.post('/place-order', authValidation, orderController.placeProductOrder);

//user
router.post('/update-cart', authValidation, cartValidation, productController.updateCartProductNumber);

router.post('/remove-from-favourite', authValidation, productIdValidation, productController.removeFavouriteProduct);

router.post('/add-to-favourite', authValidation, productIdValidation, productController.addToFavourite);

router.post('/remove-from-cart', authValidation, productIdValidation , productController.removeProductFromCart);

router.post('/add-to-cart', authValidation, cartValidation, productController.addProductToUserCart);

//productReview
router.get('/get-product-review/:productId', productReviewController.getProductReviewById);

router.post('/add-review',authValidation, productReviewController.addNewReview);

//questionAnswer
router.get('/get-product-question/:productId', questionAnswerController.getQuestionAnswerById);

router.post('/add-reply', authValidation, replyValidation, questionAnswerController.addReplyToQuestion);

router.post('/add-question', authValidation, questionValidation, questionAnswerController.addNewQuestion);

//brand
router.post('/add-brand', brandController.addNewBrand);

//products
router.get('/get-product-detail/:productId', productController.getProductDetail);

router.get('/get-products', productController.getProducts);

router.post('/add', productController.addProduct);

module.exports = router;