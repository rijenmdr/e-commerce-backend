const QuestionAnswer = require('../model/questionAnswer');
const Product  = require('../model/product');

exports.addNewQuestion = async(req, res, next) => {
    const {
        productId,
        question
    } = req.body;

    const {
        id
    } = req.user;

    try {
        const newQuestion = new QuestionAnswer({
            userId: id,
            productId,
            question
        })

        await newQuestion.save();

        const product = await Product.findById(productId);

        product.questionAnswer.push(newQuestion);

        await product.save();

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Question Added Successfully",
            question: newQuestion
        });
    } catch (err) {
        const error = new Error(err);
        next(error); 
    }
} 

exports.addReplyToQuestion = async(req, res, next) => {
    const {
        questionId,
        reply
    } = req.body;

    const {
        id
    } = req.user;

    
    try {
        const question = await QuestionAnswer.findOne({ _id: questionId }).populate('productId', "merchantId");
        
        console.log(question?.productId?.merchantId?.toString(), id)
        if((question?.productId?.merchantId?.toString()) !== id){
            return res.status(403).json({
                status: 403,
                statusMessage: "Fail",
                message: "You don't have permission to leave reply."
            });
        }
        
        await QuestionAnswer.findByIdAndUpdate(questionId, {
            reply
        });

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Answer Added Successfully",
            reply
        })
        
    } catch (err) {
        const error = new Error(err);
        next(error);
    }
}

exports.getQuestionAnswerById = async(req, res, next) => {
    const productId = req.params.productId;
    const limit = req.query.limit;

    try {
        const questionAnswer = await QuestionAnswer.find({
            productId
        }).populate("userId","name").limit(parseInt(limit)).sort({createdAt: -1});

        const questionCount = await QuestionAnswer.find({
            productId
        }).count()

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Fetched Question and Answer Successfully",
            questionAnswer,
            totalQuestion: questionCount
        });

    } catch (err){
        const error = new Error(err);
        next(error); 
    }
}