const Product = require('../model/product');
const Merchant = require('../model/merchant');
const User = require('../model/user');

exports.addProduct = async (req, res, next) => {
    const {
        productId,
        name,
        productImages,
        previewImg,
        shortDescription,
        actualPrice,
        description,
        categoryId,
        brandId,
        onStock,
        soldIn,
        serviceType,
        extraInformation,
        numberForDelivery,
        timeForDelivery,
        shippingCost
    } = req.body;

    const merchantId = '61d7d52e976f14479e3a33d6'

    try {
        const newProduct = new Product({
            productId,
            name,
            merchantId,
            productImages,
            previewImg,
            shortDescription,
            actualPrice,
            description,
            categoryId,
            brandId,
            onStock,
            soldIn,
            serviceType,
            extraInformation,
            deliveryIn: {
                numberForDelivery,
                timeForDelivery
            },
            shippingCost
        })

        await newProduct.save();

        const user = await Merchant.findOne({
            userId: merchantId
        });

        user.products.push(newProduct);

        await user.save();

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Product Created Successfully"
        })
    } catch (e) {
        const error = new Error(e);
        next(error);
    }
}


exports.getProducts = async(req, res, next) => {
    const search = req.query.search;
    const categoryId = req.query.categoryId;
    const service = req.query.service;
    const rating = req.query.rating;
    const min = req.query.min;
    const max= req.query.max;
    const page = req.query.page;
    const sort = req.query.sortBy;

    let skip = (page - 1) * 9;
    const limit = 9;
    let sortBy, serviceData;
    let brands = [];

    if(service) {
        serviceData = service.split("-")
    }

    console.log(min,max)

    let query = {}

    if(search) {query.name =  {"$regex": `${search}`, "$options": "i"}}

    if(categoryId) { query.categoryId = categoryId }

    if(serviceData) {
        serviceData.length === 1 ?
            query.serviceType = serviceData :
            query.serviceType = "b"
    }

    if(rating) {
        query.averageRating = {"$gte": rating}
    }

    if(min) {
        query.actualPrice = {"$gte": min}
    }

    if(max) {
        query.actualPrice = {"$lte": max}
    }

    if (min && max) {
        query.actualPrice = {"$gte": min,"$lte": max}
    }

    // filter = search && categoryId ? 
    //             { "$and":[{"categoryId":categoryId}, {"name": {"$regex": `${search}`, "$options": "i"}}]} :
    //             categoryId ?
    //                 {"categoryId":categoryId} :
    //             search ? 
    //                 {"name": {"$regex": `${search}`, "$options": "i"}} :
    //                 {}

    console.log(query)

    const [field, order] = sort.split('|');

    if(field === "date") {
        sortBy = {createdAt: order}
    } else {
        sortBy = {actualPrice: order}
    }


    try {
        const products = await Product.find(query)
                            .populate("brandId")
                            .skip(skip)
                            .limit(limit)
                            .sort(sortBy);
        const totalProducts = await Product.find(query).count();

        // if(Object.keys(query).length!==0) {
        //     brands = products.map(product=>product?.brandId)
        //                 .filter((value, index, self)=> self.indexOf(value) === index);
        // }

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Product fetched Successfully",
            products,
            totalProducts,
            // brands
        })
    } 
    catch (err) {
        const error = new Error(err);
        next(error);
    }
}

exports.getProductDetail = async (req, res, next) => {
    const productId = req.params.productId;
    const limit = 4

    try {
        const product = await Product.findOne({ _id: productId }).populate("categoryId").populate("merchantId","name");
        const relatedProducts = await Product.find(
            {
                "categoryId": product.categoryId,
                "_id":{ $not: {$eq: productId}}
            },
           ).limit(limit)
        if (product && relatedProducts) {
            res.status(201).json({
                status: 201,
                statusMessage: "Success",
                message: "Product fetched Successfully",
                product,
                relatedProducts
            })
        }
    } catch (err) {
        const error = new Error(err);
        next(error); 
    }
}

exports.addProductToUserCart = async(req, res, next) => {
    const {
        id
    } = req.user;

    const {
        productId,
        numberOfItem
    } = req.body

    try {
        const user = await User.findById(id);

        if(user.cart.length >= 10) {
            return res.status(400).json({
                status: 400,
                statusMessage: "Fail",
                message: "Cart is full"
            });
        }

        const exists = user.cart.findIndex(el => el?.id?.toString() === productId);
        console.log("exists",exists)

        if(exists !== -1) {
            return res.status(409).json({
                status: 409,
                statusMessage: "Fail",
                message: "Product Already Exists in Cart"
            })
        } 

        const product = await Product.findById(productId);

        if(!product) {
            return res.status(404).json({
                status: 404,
                statusMessage: "Fail",
                message: "Product Not Found"
            })
        }

        if(product?.onStock < numberOfItem) {
            return res.status(400).json({
                status: 400,
                statusMessage: "Fail",
                message: "Number of item should be less than or equal to stock"
            })
        }

        const cartProduct = {
            id: product?.id,
            name: product?.name,
            previewImg: product?.previewImg,
            averageRating: product?.averageRating,
            actualPrice: product?.actualPrice,
            onStock: product?.onStock,
            soldIn: product?.soldIn,
            shippingCost: product?.shippingCost,
            numberForDelivery: product?.deliveryIn?.numberForDelivery,
            timeForDelivery: product?.deliveryIn?.timeForDelivery,
            numberOfItem: numberOfItem
        }

        user.cart.push(cartProduct);

        await user.save();

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Product Added To cart Successfully",
            cart: user?.cart
        })
    } catch(err){
        const error = new Error(err);
        next(error);
    }
}

exports.removeProductFromCart = async(req, res, next) => {
    const { productId } = req.body;

    const {
        id
    } = req.user;

    try {
        const user = await User.findById(id);

        const exists = user.cart.findIndex(el => el?.id?.toString() === productId);
        console.log("exists",exists)

        if(exists === -1) {
            return res.status(409).json({
                status: 409,
                statusMessage: "Fail",
                message: "Product Already Doesn't Exist in Cart"
            })
        } 

       await User.updateOne({_id: id},{ $pull: { cart: { id: productId }} })

       const updatedCart = user.cart.filter(cart => cart?.id.toString() !== productId)

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Product Removed from cart Successfully",
            cart: updatedCart
        })

    } catch(err) {
        const error = new Error(err);
        next(error);
    }
}

exports.updateCartProductNumber = async(req, res, next) => {
    const {
        id
    } = req.user;

    const {
        productId,
        numberOfItem
    } = req.body;

    try {
        const user = await User.findById(id);

        const exists = user.cart.findIndex(el => el?.id?.toString() === productId);
        console.log("exists",exists)

        if(exists === -1) {
            return res.status(409).json({
                status: 409,
                statusMessage: "Fail",
                message: "Product Already Doesn't Exist in Cart"
            })
        } 

        let updatedCart = user.cart;
        updatedCart[exists].numberOfItem = numberOfItem;

       await User.updateOne({_id: id},{ cart: updatedCart});

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Number of items in cart updated successfully",
            cart: updatedCart
        });

    } catch (err) {
        const error = new Error(err);
        next(error);
    }
}

//favourite products

exports.addToFavourite = async(req, res, next) => {
    const {
        id
    } = req.user;
    

    const {
        productId
    } = req.body

    try {
        const user = await User.findById(id);

        const exists = user.favouriteProducts.findIndex(el => el?.toString() === productId);
        console.log("exists",exists)

        if(exists !== -1) {
            return res.status(409).json({
                status: 409,
                statusMessage: "Fail",
                message: "Product Already Exists as Favourite"
            })
        } 

        const product = await Product.findById(productId);

        if(!product) {
            return res.status(404).json({
                status: 404,
                statusMessage: "Fail",
                message: "Product Not Found"
            })
        }

        user.favouriteProducts.push(productId);

        await user.save();

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Product added as favourite Successfully",
        })
    } catch(err){
        const error = new Error(err);
        next(error);
    }
}


exports.getUserFavouriteProducts = async(req, res, next) => {
    const {
        id
    } = req.user;

    try {
        const user = await User.findById(id).populate("favouriteProducts");

        return  res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Favourite Products Fetched Successfully",
            wishlist: user?.favouriteProducts
        })
    } catch (err) {
        const error = new Error(err);
        next(error);
    }
}

exports.removeFavouriteProduct = async(req, res, next) => {
    const { productId } = req.body;

    const {
        id
    } = req.user;

    try {
        const user = await User.findById(id);

        const exists = user.favouriteProducts.findIndex(el => el?.toString() === productId);
        console.log("exists",exists)

        if(exists === -1) {
            return res.status(409).json({
                status: 409,
                statusMessage: "Fail",
                message: "Product Doesn't Exist in Wishlist"
            })
        } 

       await User.updateOne({_id: id},{ $pull: { favouriteProducts: productId } })

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Product Removed from wishlist Successfully",
        })

    } catch(err) {
        const error = new Error(err);
        next(error);
    }
}
