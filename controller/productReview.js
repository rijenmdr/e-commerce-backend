const ProductReview = require('../model/productReview');
const Product = require('../model/product');
const ObjectId = require('mongodb').ObjectId;

exports.addNewReview = async (req, res, next) => {
    const {
        productId,
        rating,
        review
    } = req.body

    const {
        id
    } = req.user;

    try {
        const ratingExists = await ProductReview.findOne({
            productId,
            userId: id
        });
        if (ratingExists) {
            console.log("Rating Already exists")
            return res.status(409).json({
                status: 409,
                statusMessage: "Fail",
                message: "Rating for this user already exists Already Exists"
            })
        }
        const newReview = new ProductReview({
            productId,
            userId: id,
            rating,
            review
        });

        const newData = await newReview.save();

        const averageRating = await ProductReview.aggregate([
            { $match: { "productId":  ObjectId(productId) } },
            {
                $group:
                {
                    "_id":"_id",
                    averageRate: { $avg: "$rating" }
                }
            }
        ]);

        await Product.updateOne(
            { "_id" : productId }, 
            {  
                $set: {
                    averageRating: averageRating[0]?.averageRate.toFixed(1),
                },
                $push: {
                    reviews: newData?._id
                }
            }
        );

        console.log(averageRating);

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Review Added Successfully",
        })

    } catch (err) {
        const error = new Error(err);
        next(error);
    }
}

exports.getProductReviewById = async(req, res, next) => {
    const productId = req.params.productId;
    const limit = req.query.limit;

    try {
        const productReview = await ProductReview.find({
            productId
        }).populate("userId", "name profileImage").limit(parseInt(limit)).sort({createdAt: -1});

        // const reviewCount = await ProductReview.find({
        //     productId
        // });

        const ratingCount = await ProductReview.aggregate([
            { $match: { "productId":  ObjectId(productId) } },
            {
                $group:
                {
                    "_id":"$productId",
                    "5_star_ratings": {
                        "$sum": {
                            "$cond": [ { "$eq": [ "$rating", 5 ] }, 1, 0 ]
                        }
                    },
                    "4_star_ratings": {
                        "$sum": {
                            "$cond": [ { "$eq": [ "$rating", 4 ] }, 1, 0 ]
                        }
                    },
                    "3_star_ratings": {
                        "$sum": {
                            "$cond": [ { "$eq": [ "$rating", 3 ] }, 1, 0 ]
                        }
                    },
                    "2_star_ratings": {
                        "$sum": {
                            "$cond": [ { "$eq": [ "$rating", 2 ] }, 1, 0 ]
                        }
                    },
                    "1_star_ratings": {
                        "$sum": {
                            "$cond": [ { "$eq": [ "$rating", 1 ] }, 1, 0 ]
                        }
                    },
                }
            }
        ]);

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Fetched Reviews Successfully",
            productReview,
            totalReviews: ratingCount[0]
        });

    } catch (err){
        const error = new Error(err);
        next(error); 
    }
}

exports.getAllUserReviews = async(req, res, next) => {
    const {
        id
    } = req.user;
    
    try {
        const reviews = await ProductReview.find({userId: id}).populate("productId", "name previewImg");
        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "User Reviews Fetched Successfully",
            reviews,
        });
    } catch (err) {
        const error = new Error(err);
        next(error); 
    }
}