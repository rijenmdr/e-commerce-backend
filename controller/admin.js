const SiteSetting = require('../model/siteSetting');
const Product = require('../model/product');
const FeaturedBlog = require('../model/featuredBlog');
const Testimony = require('../model/testimony');

exports.addSiteSettings = async(req, res, next) => {
    const {
      nav,
      categoryMenu,
      carouselImages,
      bestSellingCategory,
      bestSellingProducts,
      featuredTitle,
      featuredProductTitles,
      featuredProduct,
      testimony,
      popularProduct,
      primaryFeaturedBlogs,
      secondaryFeaturedBlogs,
      productTags  
    } = req.body;

    try {
        const siteSettings = new SiteSetting({
            nav,
            categoryMenu,
            carouselImages,
            bestSellingCategory,
            bestSellingProducts,
            featuredTitle,
            featuredProductTitles,
            featuredProduct,
            testimony,
            popularProduct,
            primaryFeaturedBlogs,
            secondaryFeaturedBlogs,
            productTags
        });

        await siteSettings.save();

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Site Settings Created Successfully"
        });
    } catch (err) {
        const error = new Error(err);
        next(error); 
    }
}

exports.addTestimony = async(req, res, next) => {
    const {
        name,
        profileImage,
        quote
    } = req.body;

    try {
        const testimony = new Testimony({
            name,
            profileImage,
            quote
        });

        await testimony.save();

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Testimony Added Successfully"
        });
    } catch (err) {
        const error = new Error(err);
        next(error); 
    }
}

exports.getHomeDetails = async(req, res, next) => {
    try {
        const bestSellingProducts = await Product.find({},{
            productImages: 0,
            description: 0,
            isPublished: 0,
            extraInformation: 0,
            questionAnswer: 0,
            reviews: 0,
            unitSold: 0
        }).sort({ unitSold: -1 }).limit(3);

        // const bestSellingCategory = await Product.find({},{
        //    categoryId: 1
        // }).populate('categoryId').sort({ unitSold: -1 }).limit(5);

        const popularProducts = await Product.find({},{
            productImages: 0,
            description: 0,
            isPublished: 0,
            extraInformation: 0,
            questionAnswer: 0,
            reviews: 0,
            unitSold: 0
        }).sort({ averageRating: -1 }).limit(4);

        const featuredBlogs = await FeaturedBlog.find({
            type: "home"
        }).populate({
            path: "blogId", 
            // select: "title c createdAt",
            populate:[{
                path: "authorId",
                model: "User",
                select:"name profileImage"
            },
            {
                path: "categoryId",
                model: "Category",
            }]
        }).sort({ priority: 1 });

        const featuredProducts = await Product.find({categoryId: "61c5e2df649e325820ffaf19"},{
            productImages: 0,
            description: 0,
            isPublished: 0,
            extraInformation: 0,
            questionAnswer: 0,
            reviews: 0,
            unitSold: 0
        }).limit(3);

        const testimony = await Testimony.find({},{
            createdAt:0,
            updatedAt:0
        });

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Fetched Successfully",
            bestSellingProducts,
            popularProducts,
            featuredBlogs,
            featuredProducts,
            testimony
        });
    } catch (err) {
        const error = new Error(err);
        next(error); 
    }
}