const User = require('../model/user');
const bcrypt = require('bcrypt');
const Merchant = require('../model/merchant');
const Order = require('../model/order');
const { OAuth2Client } = require('google-auth-library');
const { cloudinary } = require('../utils/cloudinary');

const jwt = require('jsonwebtoken');
require('dotenv').config();

const client = new OAuth2Client(process.env.CLIENT_ID)

exports.registerUser = async (req, res, next) => {
    const {
        firstName,
        lastName,
        email,
        password,
        mobileNo,
        gender,
        country,
        city,
        // role,
        street,
        dob
    } = req.body

    const roleId = 3;
    let name;

    if (roleId === 3) {
        name = `${firstName.trim()} ${lastName.trim()}`;
    } else {
        name = `${firstName.trim()}`
    }

    try {
        const user = await User.findOne({ email })
        if (user) {
            console.log("User Already exists")
            res.status(409).json({
                status: 409,
                statusMessage: "Fail",
                message: "User Already Exists"
            })
        } else {
            const hashedPassword = await bcrypt.hash(password, 12);

            const newUser = new User({
                name,
                email,
                password: hashedPassword,
                mobileNo,
                gender,
                address: {
                    country,
                    city,
                    street
                },
                dob,
                role: roleId,
                isVerified: false
            })

            const user = await newUser.save();

            if (roleId === 2) {
                const merchantUser = new Merchant({
                    userId: user._id
                })
                await merchantUser.save();
            }

            res.status(201).json({
                status: 201,
                message: "User Registed Successfully",
                name,
                email
            })
        }
    }
    catch (err) {
        const error = new Error(err);
        next(error);
    }
}

exports.loginUser = async (req, res, next) => {
    const {
        loginId,
        password,
        fcmToken
    } = req.body;

    let query = '';

    if (fcmToken === "4275") {
        query = {$and: [{ "email": loginId, "isGoogleUser": {$ne: true} }]}
    } else if (fcmToken === "4274") {
        query = {$and: [{ "mobileNo": loginId, "isGoogleUser": {$ne: true} }]}
    } else {
        return res.status(400).json({
            status: 400,
            statusMessage: "Fail",
            message: "Invalid fcm token"
        })
    }

    try {

        const user = await User.findOne(query);

        if (!user) {
            return res.status(404).json({
                status: 404,
                statusMessage: "Fail",
                message: "User Not Found"
            })
        }

        const hashCompare = await bcrypt.compare(password, user.password);

        if (!hashCompare) {
            return res.status(401).json({
                status: 401,
                message: "Invalid Credentials"
            });
        }

        const token = jwt.sign({
            id: user?._id,
            email: user?.email,
            name: user?.name
        }, process.env.JWT_SECRET_KEY, { expiresIn: "24h" });

        const userDetail = {
            id: user?._id,
            name: user?.name,
            email: user?.email,
            mobileNo: user?.mobileNo,
            address: user?.address,
            dob: user?.dob,
            gender: user?.gender,
            cart: user?.cart,
            access_token: token
        };

        return res.status(201).json({
            status: 201,
            message: "User Login Successfully",
            userDetail
        })

    } catch (err) {
        const error = new Error(err);
        next(error);
    }
}

exports.forgetPassword = async (req, res, next) => {
    const {
        mobileNo,
        password
    } = req.body;

    try {
        const user = await User.findOne({ mobileNo });
        console.log(user)
        if (!user) {
            return res.status(404).json({
                status: 404,
                statusMessage: "Fail",
                message: "User Not Found"
            })
        }
        const hashedPassword = await bcrypt.hash(password, 12);

        await User.updateOne(
            { "_id": user?._id },
            {
                $set: {
                    password: hashedPassword,
                }
            }
        );

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Password Changed Successfully"
        })
    } catch (err) {
        const error = new Error(err);
        next(error);
    }
}

exports.userExists = async (req, res, next) => {
    const {
        mobileNo
    } = req.body;

    try {
        const user = await User.findOne({ mobileNo });
        console.log(user)
        if (user) {
            return res.status(201).json({
                status: 201,
                statusMessage: "Success",
                number: mobileNo,
                state: "already",
                message: "Mobile Number Already Registered"
            });
        } else {
            return res.status(201).json({
                status: 201,
                statusMessage: "Success",
                number: mobileNo,
                state: "new",
                message: "Unique Mobile Number"
            })
        }
    } catch (err) {
        const error = new Error(err);
        next(error);
    }
}

exports.loginWithGoogle = async(req, res, next) => {
    const { token }  = req.body;
    try {
        const ticket = await client.verifyIdToken({
            idToken: token,
            audience: process.env.CLIENT_ID
        });
        const { name, email, picture } = ticket.getPayload();   
        console.log(name, email, picture);
    
        const user = await User.findOne({email});
        
        if(user) {
            const token = jwt.sign({
                id: user?._id,
                email: user?.email,
                name: user?.name
            }, process.env.JWT_SECRET_KEY, { expiresIn: "24h" });
    
            const userDetail = {
                id: user?._id,
                name: user?.name,
                email: user?.email,
                mobileNo: user?.mobileNo,
                address: user?.address,
                dob: user?.dob,
                gender: user?.gender,
                cart: user?.cart,
                access_token: token
            };
    
            return res.status(201).json({
                status: 201,
                message: "User Login Successfully",
                userDetail
            })
        } else {
            const createUser = new User({
                name,
                email,
                profileImage: picture,
                role: 3,
                isVerified: false,
                isGoogleUser: true
            });
            const newUser = await createUser.save();
            const token = jwt.sign({
                id: newUser?._id,
                email: newUser?.email,
                name: newUser?.name
            }, process.env.JWT_SECRET_KEY, { expiresIn: "24h" });
    
            const userDetail = {
                id: newUser?._id,
                name: newUser?.name,
                email: newUser?.email,
                mobileNo: newUser?.mobileNo,
                address: newUser?.address,
                dob: newUser?.dob,
                gender: newUser?.gender,
                cart: newUser?.cart,
                access_token: token
            };
    
            return res.status(201).json({
                status: 201,
                message: "User Login Successfully",
                userDetail
            })
        }
    } catch(err){
        const error = new Error(err);
        next(error);
    }
}

exports.getCurrentUser = async(req, res, next) => {
    const {
        id
    } = req.user;
    console.log(id)
    try {
        const user = await User.findById(id);

        const currentUser = {
            id: user?._id,
            name: user?.name,
            email: user?.email,
            profileImage: user?.profileImage,
            mobileNo: user?.mobileNo,
            address: user?.address,
            dob: user?.dob,
            gender: user?.gender,
            cart: user?.cart,
        };

        res.status(200).json({
            status: 201,
            statusMessage: "success",
            message: "Current User Fetched Successfully",
            currentUser
        });
    } catch (err) {
        const error = new Error(err);
        next(error);
    }
}

exports.updateUserProfile = async(req, res, next) => {
    const {
        name,
        email,
        dob,
        gender,
        street,
        city,
        country
    } = req.body;

    const {
        id
    } = req.user;

    const address = {
        street,
        city,
        country
    }

    const updatedUser = {
        name,
        email,
        dob,
        gender,
        address
    }

    try {
        await User.updateOne({
            _id: id
        },{
            $set: updatedUser
        });

        const user = await User.findById(id);

        const currentUser = {
            id: user?._id,
            name: user?.name,
            email: user?.email,
            mobileNo: user?.mobileNo,
            profileImage: user?.profileImage,
            address: user?.address,
            dob: user?.dob,
            gender: user?.gender,
            cart: user?.cart,
        };

        res.status(200).json({
            status: 201,
            statusMessage: "success",
            message: "User Profile Updated Successfully",
            currentUser
        });

    } catch(err) {
        const error = new Error(err);
        next(error);
    }   
} 

exports.uploadProfilePicture = async (req, res, next) => {
    const image = req.body.image;

    console.log(image)

    const {
        id,
        name
    } = req.user;

    try {
        const uploadedImage = await cloudinary.uploader.upload(image, {
            upload_preset: 'freshnese',
            folder:id,
            public_id:name
        })
        console.log('uploded', uploadedImage)

        await User.updateOne(
            {
                _id: id
            },
            {
                profileImage: uploadedImage?.secure_url
            }
        )

        const user = await User.findOne({_id: id});

        const currentUser = {
            id: user?._id,
            name: user?.name,
            email: user?.email,
            mobileNo: user?.mobileNo,
            profileImage: user?.profileImage,
            address: user?.address,
            dob: user?.dob,
            gender: user?.gender,
            cart: user?.cart,
        };

        return res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Profile Image Uploaded Successfully",
            currentUser
        })
    } catch (err) {
        const error = new Error(err);
        next(error);
    }
}