const Product = require('../model/product');
const Order = require('../model/order');
const User = require('../model/user');
const moment = require('moment');
const ObjectId = require('mongodb').ObjectId;

exports.placeProductOrder = async(req, res, next) => {
    const {
        id
    } = req.user;

    const {
        products,
        name,
        email,
        mobile,
        street,
        city,
        country,
        zip,
        additionalInformation
    } = req.body;

    let stockValid = true;
    let amount = 0;

    try {
        const address = `${street}, ${city}, ${country}`;

        const productIds = products.map(product => product.id);

        const orderedProducts = await Product.find({_id: productIds},{
            _id: 1,
            name: 1,
            actualPrice: 1,
            onStock: 1,
            shippingCost: 1,
            deliveryIn: 1
        });

       products.map((product, index) => {
           if(Number(product?.numberOfItem) > orderedProducts[index]?.onStock) {
               stockValid = false;
               return;
           }
       })

       if(!stockValid) {
        return res.status(400).json({
            status: 400,
            statusMessage: "Fail",
            message: `Quantity cannot be greater than actual stock`
        })
       }

       amount = orderedProducts.reduce((prevVal, currentVal, index) => {
            return prevVal + currentVal?.actualPrice * products[index].numberOfItem
       }, 0)

       const maxShippingCost = Math.max(...orderedProducts.map((product) => product.shippingCost)) 

       const maxShippingDate = Math.max(...orderedProducts.map((product) => product?.deliveryIn?.numberForDelivery))

       const expectedDate = new Date();
       expectedDate.setDate(expectedDate.getDate() + maxShippingDate);

       const formatedDate = moment(expectedDate).format('ll')
       
       const newOrder = new Order({
            productIds,
            userId: id,
            name,
            email,
            mobile,
            address,
            zip,
            additionalInformation,
            amount: amount + maxShippingCost,
            hasPaid: false,
            expectedDate: formatedDate,
            paymentMethod: "c", 
       });

       await newOrder.save();

       let bulkArr = [];

       for (const i of products) {
           bulkArr.push({
               updateOne: {
                   "filter": { "_id": ObjectId(i.id) },
                   "update": { $inc: { "unitSold": 1 , "onStock": - i.numberOfItem } }
               }
           })
       }
       
       Product.bulkWrite(bulkArr);

       if(products.length >= 2) {
           await User.updateOne({ _id: id }, { $set: { 'cart': [] } })
       } 

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Product has been purchased successfully"
        })
    } catch (err) {
        const error = new Error(err);
        next(error);
    }
}

exports.getUserPurchases = async(req, res, next) => {
    const {
        id
    } = req.user;

    try {
        const purchases = await Order.find({userId : id})
                                    .populate("productIds", "name previewImg");

        res.status(200).json({
            status: 201,
            statusMessage: "success",
            message: "User Purchases Fetched Successfully",
            purchases
        });

    } catch(err) {
        const error = new Error(err);
        next(error);
    }
}

exports.cancelOrder = async(req, res, next) => {
    const {
        orderId,
        reason
    } = req.body;

    const {
        id
    } = req.user;

    try {
        const cancelDate = new Date();

        await Order.updateOne({_id: orderId}, {$set: {
            hasCanceled: true,
            cancelledDate: cancelDate,
            reason
        }});

        return res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Your order has been cancelled successfully"
        })

    } catch(err) {
        const error = new Error(err);
        next(error);
    }
}

exports.getAllUserPurchasedProducts = async(req, res, next) => {
    const {
        id
    } = req.user;

    let products = [];

    try {
        const purchases = await Order.find({userId: id}, { productIds: 1 }).populate("productIds", "name previewImg averageRating");
        
        console.log(purchases)
        products = purchases.map((a) => a.productIds);

        let mergedProducts = [].concat.apply([], products);
        
        const purchasedProducts = Array.from(new Set(mergedProducts));

        console.log(purchasedProducts)

        res.status(201).json({
            status: 201,
            statusMessage: "Success",
            message: "Purchases Products Fetched Successfully",
            purchasedProducts: purchasedProducts
        })
    } catch(err) {
        const error = new Error(err);
        next(error);
    }
}